## Creation of Article

### Request

#### HTTP headers

```
{
  "Authorization": "Bearer a1RS0I6jV6W8E1wDgrQ6czw136oW442jyTUNX9aPYieF1zwy1FWI3Jvctlul"
}
```

#### Body

```
mutation {
  createArticle(
    title:"Building a GraphQL Server with Laravel"
    content:"In case you're not currently familiar with it, GraphQL is a query language used to interact with your API..."
  ) {
    id
    author {
      id
      email
    }
  }
}
```

### Response

```
{
  "data": {
    "createArticle": {
      "id": "56",
      "author": {
        "id": "1",
        "email": "graphql@test.com"
      }
    }
  }
}
```
