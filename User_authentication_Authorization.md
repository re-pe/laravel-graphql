## Creation of user

### Request

mutation {
  createUser(
    name:"John Doe"
    email:"john.doe@example.com"
    password: "secret"
  ) {
    id
    name
    email
  }
}

### Response

{
  "data": {
    "createUser": {
      "id": "12",
      "name": "John Doe",
      "email": "john.doe@example.com"
    }
  }
}

## Authentication:

### Request:

mutation {
  login(email:"graphql@test.com", password:"secret")
}

### Response:

{
  "data": {
    "login": "a1RS0I6jV6W8E1wDgrQ6czw136oW442jyTUNX9aPYieF1zwy1FWI3Jvctlul"
  }
}

## Autorization:

### Request:

#### HTTP headers:

{
  "Authorization": "Bearer a1RS0I6jV6W8E1wDgrQ6czw136oW442jyTUNX9aPYieF1zwy1FWI3Jvctlul"
}

#### Body:

{
  me {
    email
    articles {
      id
      title
    }
  }
}

### Response:

{
  "data": {
    "me": {
      "email": "graphql@test.com",
      "articles": [
        {
          "id": "1",
          "title": "Cum labore sint commodi eos eveniet mollitia vitae."
        },
        {
          "id": "2",
          "title": "Qui libero facere et et inventore."
        },
        {
          "id": "3",
          "title": "Omnis dolorem dolor enim tenetur aut labore."
        },
        {
          "id": "4",
          "title": "Praesentium quidem quia aperiam odit at blanditiis ullam."
        },
        {
          "id": "5",
          "title": "Quam necessitatibus ea eius dolor."
        }
      ]
    }
  }
}
